# Week8 Mini Project


The purpose of this project is to create a command-line tool for handling CSV files. Through this tool, users can specify the input CSV file, the subject for filtering records, and the grade range. The tool will read the CSV file, filter records based on the provided criteria, and print the filtered records to the console. The main goal of this tool is to allow users to easily extract records from CSV files based on specific conditions to meet their data processing and analysis needs. By providing a command-line interface and corresponding data processing functionality, this tool serves as a flexible and efficient data processing tool, aiding users in quickly processing and analyzing large amounts of CSV data.

## Project Setup

1. Install Rust: Ensure that Rust is installed on your system. You can install it via rustup, Rust's version manager.

2. Create a New Project: Use Cargo, Rust's package manager, to create a new project. Open your terminal and run:
```
cargo new rust_command-line_tool
```

Or build the existing code using the following command:
```
cargo build
```

3. Add Dependencies: Add the necessary dependencies to your Cargo.toml file. For this project, you'll need dependencies such as clap for command-line argument parsing and csv for CSV file handling. Your Cargo.toml might look like this:
```
serde_json = "1.0.113"
serde = { version = "1.0.196", features = ["derive"] }
csv = "1.1.6"
clap = "2.27.1"
tokio = { version = "1.37.0", features = ["rt", "rt-multi-thread", "macros"] }
```

4. Define Data Structures: Define any necessary data structures for representing the data in your CSV files. In this project, StudentRecord struct has been already defined. You need to create a template CSV file under the data dictionary using the following format. Notice that a template `data/test1.csv` has been already created. 

![Screenshot 2024-03-29 at 7.34.22 AM.png](image%2FScreenshot%202024-03-29%20at%207.34.22%20AM.png)

## Tool functionality
The code defines a command-line tool for processing CSV files. It utilizes the `clap` crate to parse command-line arguments, allowing users to specify the input CSV file, the subject to filter records by, as well as the lower and upper bounds for the grade range. After parsing the arguments, the tool reads the CSV file, filters the records based on the provided criteria, and prints the filtered records to the console.

Navigate to the root directory. Run the following code to test the function. Remember to replace <file>, <high>, <low> and <subject> with the data. 

```shell
./rust_command-line_tool <file> --high <high> --low <low> --subject <subject>
```

Here we filter the students who have a Math grade between 80 and 90 based on the test1.csv:

![Screenshot 2024-03-29 at 7.40.29 AM.png](image%2FScreenshot%202024-03-29%20at%207.40.29%20AM.png)

## Data processing
The `process_csv` function reads the CSV file specified by the user and parses its contents into `StudentRecord` structs. It utilizes the `csv` crate to handle CSV parsing. Each CSV record is converted into a StudentRecord, which consists of fields like name, age, grade, gender, and subject.

The `filter_records` function takes the parsed `StudentRecord` vector, along with the subject, lower bound, and upper bound for the grade range, and filters the records based on these criteria. It returns a vector containing only the records that match the specified subject and fall within the grade range.

## Testing implementation
The code includes a test function named `test_grade_filter` within the tests module. This test asynchronously reads a test CSV file, applies filtering based on a predefined subject and grade range, and verifies that the filtering logic works correctly. It checks if the filtered records match the expected results and asserts the correctness of the output. This test ensures that the filtering functionality behaves as expected and can be used to validate the correctness of the tool's processing logic.

The following testing code is to test a filtering function (`filter_records()`) applied to records loaded from a CSV file. It sets the subject to "Math" and specifies a lower bound of 80 and an upper bound of 90. Then, it processes the CSV file, filters records where the grade falls between 80 and 90, and checks if the filtered records match the expected results using assertions. If the assertions pass, it prints "Pass the tests!!!".

```rust
#[tokio::test]
async fn test_grade_filter() {
    let file_path = "data/test1.csv";
    let subject = "Math";
    let low_bound = "80";
    let high_bound = "90";

    let records = process_csv(file_path).unwrap();
    let filtered_records = filter_records(records, subject, low_bound, high_bound).unwrap();

    println!("{:?}", filtered_records);
    assert_eq!(filtered_records.len(), 2);
    assert_eq!(filtered_records[0].name, "John");
    assert_eq!(filtered_records[0].grade, 88.2);
    assert_eq!(filtered_records[1].name, "Jane");
    assert_eq!(filtered_records[1].grade, 89.1);
    println!("Pass the tests!!!");
}
```

The following figure shows the test outcomes. Try to use the following command to see the results:
```
cargo test
```
![Screenshot 2024-03-29 at 7.29.25 AM.png](image%2FScreenshot%202024-03-29%20at%207.29.25%20AM.png)

