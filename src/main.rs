use clap;
use csv;
use std::error::Error;
use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Serialize)]
struct StudentRecord {
    name: String,
    age: u8,
    grade: f32,
    gender: String,
    subject: String,
}

#[tokio::main]
async fn main() {
    let matches = clap::App::new("CLI Tool")
        .version("0.1.0")
        .author("XYC")
        .about("A command-line tool for processing CSV files")
        .arg(clap::Arg::with_name("file")
            .help("Sets the input CSV file to use")
            .required(true)
            .index(1))
        .arg(clap::Arg::with_name("subject")
            .help("Sets the subject to filter records by")
            .short("s")
            .long("subject")
            .takes_value(true)
            .required(true))
        .arg(clap::Arg::with_name("low")
            .help("Sets the lower bound for the grade range")
            .short("l")
            .long("low")
            .takes_value(true)
            .required(true))
        .arg(clap::Arg::with_name("high")
            .help("Sets the upper bound for the grade range")
            .short("h")
            .long("high")
            .takes_value(true)
            .required(true))
        .get_matches();

    let file_path = matches.value_of("file").unwrap();
    let subject = matches.value_of("subject").unwrap();
    let low_bound = matches.value_of("low").unwrap();
    let high_bound = matches.value_of("high").unwrap();

    // Process the CSV file
    let records = process_csv(file_path).unwrap();

    // Filter the records based on the grade range
    let filtered_records = filter_records(records, subject, low_bound, high_bound).unwrap();

    // Print the filtered records
    print_records(filtered_records);
}


fn process_csv(file_path: &str) -> Result<Vec<StudentRecord>, Box<dyn Error>> {
    let mut records = Vec::new();
    let mut reader = csv::Reader::from_path(file_path)?;
    for result in reader.records() {
        let record = result?;
        let student_record = StudentRecord {
            name: record[0].to_string(),
            age: record[1].parse::<u8>()?,
            grade: record[2].parse::<f32>()?,
            gender: record[3].to_string(),
            subject: record[4].to_string(),
        };
        records.push(student_record);
    }
    Ok(records)
}

// Filter the records based on the grade range
fn filter_records(records: Vec<StudentRecord>, subject: &str, low_bound: &str, high_bound: &str) -> Result<Vec<StudentRecord>, Box<dyn Error>> {
    let low_bound = low_bound.parse::<f32>()?;
    let high_bound = high_bound.parse::<f32>()?;
    let filtered_records = records.into_iter().filter(|record| {
        record.subject == subject && record.grade >= low_bound && record.grade <= high_bound
    }).collect();
    Ok(filtered_records)
}

fn print_records(records: Vec<StudentRecord>) {
    for record in records {
        println!("{:?}", record);
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[tokio::test]
    async fn test_grade_filter() {
        let file_path = "data/test1.csv";
        let subject = "Math";
        let low_bound = "80";
        let high_bound = "90";

        let records = process_csv(file_path).unwrap();
        let filtered_records = filter_records(records, subject, low_bound, high_bound).unwrap();

        println!("{:?}", filtered_records);
        assert_eq!(filtered_records.len(), 2);
        assert_eq!(filtered_records[0].name, "John");
        assert_eq!(filtered_records[0].grade, 88.2);
        assert_eq!(filtered_records[1].name, "Jane");
        assert_eq!(filtered_records[1].grade, 89.1);
        println!("Pass the tests!!!");
    }
}



